package com.eonforge.rosetta.text;

import com.eonforge.rosetta.text.Analyzer;
import com.eonforge.rosetta.text.Chunker;
import com.eonforge.rosetta.text.Chunker.TextType;
import org.junit.Assert;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


public class AnalyzerTest {  
    private static Logger log = LoggerFactory.getLogger(AnalyzerTest.class);

    @Test
    public void testAnalyze() {
        Exception error = null;
        try {
	    String sentence = 
                "this is a test string. Hopefully it is well analyzed!";
	    log.info("testAnalyze().sentence = " + sentence);
            log.info("testAnalyze().res = " + new Analyzer().analyze(sentence));
        } catch (Exception e) { e.printStackTrace(); error = e; }
        Assert.assertNull(error);
    }

    @Test
    public void testChunker() {
        Exception error = null;
        try {
	    String text = 
                "SYSTEM DESIGN DOCUMENT\n"+
"Overview\n" +
"The System Design Document describes the system requirements, operating environment, system and subsystem architecture, files and database design, input formats, output layouts, human-machine interfaces, detailed design, processing logic, and external interfaces.\n" +
"1 INTRODUCTION\n" +
"1.1 Purpose and Scope\n" +
"This section provides a brief description of the Systems Design Document’s purpose and scope.\n" +
"1.2 Project Executive Summary\n" +
"This section provides a description of the project from a management perspective and an overview of the framework within which the conceptual system design was prepared.  If appropriate, include the information discussed in the subsequent sections in the summary.\n";
	    //log.info("testChunker().text = " + text);
            log.info("testChunker().res = " + new Chunker().chunkify(text, 
                TextType.UNKNOWN));
        } catch (Exception e) { e.printStackTrace(); error = e; }
        Assert.assertNull(error);
    }
}
