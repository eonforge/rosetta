package com.eonforge.rosetta.text;

import com.eonforge.delphi.db.Client;
import com.eonforge.delphi.db.Datum;
import com.eonforge.delphi.db.model.Node;
import com.eonforge.peregrine.Config;
import com.eonforge.peregrine.http.HClient;
import com.eonforge.peregrine.parse.Json;
import com.eonforge.rosetta.index.Index;
import com.eonforge.rosetta.text.Chunker.*;

import java.io.IOException;
import java.nio.file.FileSystems;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.*;
import java.util.stream.Collectors;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class Analyzer {

    private static final Logger log = LoggerFactory.getLogger(Analyzer.class);

    static { Config.load("rosetta.properties"); }

    private static String VFS = Config.getPathProperty("vfs-dir");

    private static String SYNTAX_URL = Config.getProperty("google-syntax-url");

    private static String ENTITIES_URL = Config.getProperty(
	    "google-entities-url");

    private static List<String> OK_LABELS = Arrays.asList("DOBJ,IOBJ,NEG,NN," +
	    "NSUBJ,NSUBJPASS,NUM,NUMBER,KW,NOMC,NOMCSUBJ,NOMCSUBJPASS,ROOT".
	    split(","));

	// Format: [parent-state, keys..., node-type]
    public static Map<String, List<List<String>>> keysMap =
	    load("discourse.keys");

    private static Chunker chunker = new Chunker();

    private static Index nodeIndex = new Index(new Node());

    public String state = "";

    public Analyzer() {}

    public List<Datum> analyze(String text) throws Exception {
        log.info("analyze().text.len = " + text.length() + ", text = "+text);
	List<Datum> appInst = new ArrayList();
	List<Chunk> unmatched = new ArrayList();
	for (Chunk chunk : chunker.chunkify(text, TextType.UNKNOWN)) {
	    //chunk.searchRelatedNodes(nodeIndex);
	    if (chunk.data.isEmpty()) unmatched.add(chunk);
	    appInst.addAll(chunk.data);
	}
	if (!unmatched.isEmpty()) {
	    Map json = null;
	    if ((json = getNLPFile(text)) == null) {
		json = (Map) Json.parse(HClient.post(SYNTAX_URL, text));
		Object ents = Json.parse(HClient.post(ENTITIES_URL, text));
		Json.put(json, Json.get(ents, "entities"), "entities");
	        saveNLPFile(text, json);	
	    }
            log.info("analyze().unmatched = " + unmatched + ", json = " + json);
	    appInst.addAll(analyzeChunks(unmatched, json));
	}
        log.info("analyze().appInst.size = " + appInst.size());
	return appInst;
    }

    public List<Datum> analyzeChunks(List<Chunk> chunks, Object syntax) {
	Map<Object, Datum> nodeMap = new HashMap();
        Client<Node> nodeDb = Client.instance(new Node());
        Datum node = null;
        int i = -1;
	for (Chunk chunk : chunks) {
	    for (Datum n: chunk.parseDataFromNeighbour())
	        nodeMap.put(n.get("id"), n);
	    for (Datum n: chunk.constructNodes(nodeIndex, nodeDb)) {
                i++;
                if (node==null) node = n.merge("chunktext","","chunktitle","");
                for (String k : 
                     new String[]{"behaviour","inputs","steps","retval"}) {
                    if (n.get(k+"_0") != null) node.put(k+"_"+i, n.get(k+"_0"));
                    int j = -1;
                    while (n.get(k+"_0_"+(++j)) != null) 
                        node.put(k+"_"+i+"_"+j, n.get(k+"_0_"+j));
                }
            }
	}
	if (node != null) nodeMap.put(node.get("id"), node);
        log.info("analyzeChunks().node = " + node);
	return new ArrayList(nodeMap.values());
    }

    public List<Datum> analyze(List<String> sentences, Object syntax) {
        log.info("analyze().sentences = " + sentences + ", syntax = " + syntax);
	List<Datum> knodes = new ArrayList();
	if (syntax == null) return knodes;
	List tokens = (List) Json.get(syntax, "sentences");
	String allTxt = (String) tokens.stream().map(s -> Json.get(s,
	       "text", "content")).collect(Collectors.joining("\n"));
	int[] indices = Chunker.charIndexArray(tokens, allTxt);
	log.info("analyze().indices = " + Arrays.toString(indices) + ":" +
			indices.length+ ", tokens = " + tokens);
	List tokenGrps = splitTokens(Json.get(syntax, "tokens"), indices);
	List entGrps = splitEntities(Json.get(syntax, "entities", "entities"), 
                                    indices);
	for (int i = 0; i < sentences.size(); i++) 
	    knodes.addAll(analyze(sentences.get(i), (List) tokenGrps.get(i), 
				                    (List) entGrps.get(i)));
        log.info("analyze().sentences,syntax knodes = " + sentences + "," + 
                knodes.size());
	return knodes;
    }

    public List<Datum> analyze(String sentence, List tokens, List entities) {
        log.info("analyze().sentence, tokens, entities = " + Arrays.asList(
            sentence, tokens, entities));
	List<Datum> knodes = new ArrayList();
	for (List<String> rule : keysMap.get(state)) {
	    for (String key : rule.subList(0, rule.size())) {
		if ("<search>".equals(key)) {
		    knodes.addAll(fetchDbTokens(entities));
		} else {
		    for (Object e : entities) 
			if ((""+Json.get(e, "text", "content")).contains(key)) 
			    knodes.addAll(
				fetchDbTokens(relatedTokens(tokens, e)));
		}
	    }
	}
        for (Datum n : knodes) n.put("analyze.words", sentence);
	return knodes;
    }

    private static List<Datum> fetchDbTokens(List tokens) {
	return tokens.isEmpty()? Collections.emptyList(): 
	    com.eonforge.delphi.db.Client.instance(new Chunk()).fetch( 
		    "name = '" + tokens.stream().map(
		    e -> Json.get(e, "text", "content")).collect(
		    Collectors.joining("' OR name = '")) + "'");
    }

    private static List relatedTokens(List tokens, Object entity) {
	Object tok = tokens.stream().filter(t -> 
	    Json.get(t, "text", "beginOffset") == 
	    Json.get(entity, "text", "beginOffset")).findFirst().get();
	Map<Integer, List> tmap = (Map<Integer,List>) tokens.stream().collect(
	    Collectors.groupingBy(t -> (Integer) Json.get(t, "id")));
	List parents = new ArrayList(), related = parents;
	while (OK_LABELS.contains(Json.get(tok, "dependencyEdge", "label"))) {
	    int id = (Integer)Json.get(tok, "dependencyEdge", "headTokenIndex");
	    tok = tmap.containsKey(id)? tmap.get(id).get(0): null;
	    if (tok != null) parents.add(tok);
	}
	related.addAll(childTokens(tokens, entity));
	return related;
    }

    private static List childTokens(List tokens, Object entity) {
	if (Json.get(entity, "children") != null)
	    return (List) Json.get(entity, "children");
	tokens = (List) tokens.stream().filter(t -> OK_LABELS.contains(
		Json.get(t, "dependencyEdge", "headTokenIndex"))).
		collect(Collectors.toList());
	Map<Integer, List> tmap = (Map<Integer, List>) tokens.stream().
	    collect(Collectors.groupingBy(t -> (Integer) Json.get(t, "id")));
	for (Object tok : tokens) {
	   Object parent = tmap.get((Integer) Json.get(tok, "dependencyEdge", 
				   "headTokenIndex")).get(0);
	   if (parent != null) {
	        if (Json.get(parent, "children") == null)
		    Json.put(parent, new ArrayList(), "children");
		((List)Json.get(parent, "children")).add(tok);
	   }
	}
	return (List) Json.get(entity, "children");
    }

    private static List getSplits(int[] indices) {
	List splits = new ArrayList();
	if (indices.length > 0) {
	    int largest = indices[0] - 1;
	    for (int i : indices) {
		if (i > largest) {
		    largest = i;
	            splits.add(new ArrayList());
		}
	    }
	}
	return splits;
    }

    private static List splitTokens(Object tokensJson, int[] indices) {
	List splits = getSplits(indices);
	int i = 0;
	log.info("splitTokens.tokens = " + ((List)tokensJson).stream().map(
	    t ->Json.get(t,"text","beginOffset")).collect(Collectors.toList()));
	for (Object tok : (List) tokensJson)
	    ((List) splits.get(indices[((Long)Json.get(tok, "text",
		"beginOffset")).intValue()])).add(Json.put(tok, i++, "id"));
	return splits;
    }

    private static List splitEntities(Object entJson, int[] indices) {
	List splits = getSplits(indices);
	log.info("splitEntities.entJson = " + entJson);
	for (Object e : (List) entJson) {
	    for (Object m : (List) Json.get(e, "mentions")) {
		Json.put(m, Json.get(e, "type"), "type");
		Json.put(m, Json.get(e, "sentiment"), "sentiment");
		Json.put(m, Json.get(e, "salience"), "salience");
		((List) splits.get(indices[((Long) 
		    Json.get(m,"text","beginOffset")).intValue()])).add(m);
	    }
	}
	return splits;
    }

    private static List<String> readLines(String file) {
	try {
	    return Files.readAllLines(FileSystems.getDefault().getPath(file));
	} catch (IOException e) {
	    log.error("Error reading file " + file, e);
	}
	return Collections.emptyList();
    }

    private static Map<String, List<List<String>>> load(String discourseKeys) {
	Map<String, List<List<String>>> res = new HashMap();
	List<String> keys = null;
	for (String line : readLines(discourseKeys)) {
	    int i = 0;
	    for (String tok : line.split(",\\s*")) {
	        if (i == 0) {
		    if (!res.containsKey(tok)) res.put(tok, new ArrayList());
		    keys = new ArrayList();
		    res.get(tok).add(keys);
		} else {
		    keys.add(tok);
		}
	    }
	}
	return res;
    }

    private Map getNLPFile(String text) throws IOException {
	Path path = Paths.get(VFS + Chunker.identifier(text) + ".json");
	if (Files.exists(path))
	    return (Map) Json.parse(Files.readAllLines(path).stream().
			    collect(Collectors.joining("\n")));
	return null;
    }

    private void saveNLPFile(String text, Map json) throws IOException {
	if (text == null || text.length()==0) return;
	Path path = Paths.get(VFS + Chunker.identifier(text) + ".json");
	Files.write(path, json.toString().getBytes());
    }

}
