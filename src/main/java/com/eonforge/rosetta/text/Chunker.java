package com.eonforge.rosetta.text;

import java.io.IOException;
import java.io.StringReader;
import java.lang.StringBuilder;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;
import java.util.stream.LongStream;
import java.util.stream.Stream;
import javax.swing.text.html.HTMLEditorKit.ParserCallback;
import javax.swing.text.html.parser.ParserDelegator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.eonforge.peregrine.Config;
import com.eonforge.peregrine.http.HClient;
import com.eonforge.peregrine.parse.Json;
import com.eonforge.rosetta.index.Index;
import com.eonforge.rosetta.text.Chunk.*;

public class Chunker {

    public enum TextType { README, HTML, XML, UNKNOWN };

    private static Index index = new Index();

    private static final Logger log = LoggerFactory.getLogger(Chunker.class);

    public Chunker() {}

    public static List<Chunk> chunkify(String text, TextType type) {
	return chunkify(text, type, null, null);
    }

    public static List<Chunk> chunkify(String text, TextType type, String url,
		    String templateId) {
	List<Chunk> chunks = new ArrayList();
	if (type == TextType.README) {
  	    chunks = toReadmeChunks(Arrays.asList(
				    text.split("([\\t\\s\\n]{2,})")), url);
	    if (titleScore(chunks) > 0.25) return chunks;
	} else if (type == TextType.HTML) {
	    chunks.addAll(processLinks(text, url, 
			      "(?i)<a\\s+href=\"([^>]+).html\">[^<]*</a>",
			      "(?i)<a\\s+href=([^>]+)>Javadoc[^<]*</a>",
                              "(?i)\\[Javadoc\\]\\(([^\\)]+)\\)",
			      "(?i)<frame\\s+src=\"([^\"]+)\"[^\\>]*>"));
	}
	chunks.addAll(chunkify(type==TextType.HTML? stripHtml(text): text));
	return updateAll(chunks, templateId);
    }

    private static List<Chunk> updateAll(List<Chunk> chunks, String templateId){
	for (int i = 0; i < chunks.size(); i++) {
	    chunks.get(i).templateId = templateId;
	    chunks.get(i).prev = i==0 ? null : chunks.get(i-1);
	    chunks.get(i).next = i>=chunks.size()-1 ? null : chunks.get(i+1);
	}
	return chunks;
    }

    public static double titleScore(List<Chunk> chunks) {
	double ratio = 0, total = 0;
	for (Chunk chunk : chunks) { 
	    ratio += chunk.text == null || chunk.text.length()==0? 0:
		    chunk.title.length() / chunk.text.length();
	}
	return 1 - ratio / chunks.size();
    }

    private static List<Chunk> chunkify(String text) {
	try {
	    String vfs = Config.getPathProperty("vfs-dir");
	    Path googleCache = Paths.get(vfs + identifier(text) + ".json");
	    Object json = null, entJson = null;
	    if (Files.exists(googleCache)) {
		json = Json.parse(Files.readAllLines(googleCache).stream().
				collect(Collectors.joining("\n")));
		entJson = Json.get(json, "entities");
	    } else {
	        json = Json.parse(HClient.post(Config.getProperty(
					    "google-syntax-url"), text));
	        entJson = Json.parse(HClient.post(Config.getProperty(
					    "google-entities-url"), text));
		Files.write(googleCache, Json.put(json, entJson, "entities").
			                    toString().getBytes());
	    }
            return toChunks(text, json, entJson);
	} catch (Exception e) { e.printStackTrace(); }
	return Collections.emptyList();
    }

    
    private static List<Chunk> toChunks(String text, Object json,
        Object entJson) {
        List sentences = (List) Json.get(json, "sentences");
        log.info("toChunks().text,sentences = "+text.length()+", "+sentences);
	int[] indices = charIndexArray(sentences, text);
	List<List> sentGrps = sentGrpsByEntity(entJson, sentences, indices);
        Map<Long, String> tmap = tokenMap(json);
        Map<Long, String> lmap = lemmaMap(json);
	return sentGrps.stream().map(
	    g -> new Chunk("", content(g),entities(g,lmap),entityToks(g,tmap))).
            filter(c -> !"".equals(c.title) || !"".equals(c.text)).
	    collect(Collectors.toList());
    }

    private static String content(List sentGroup) {
	return (String) sentGroup.stream().
		map(s -> (String) Json.get(s, "text", "content")).
                filter(m -> m != null && !"".equals(m)).
	        collect(Collectors.joining("\n"));
    }

    private static List<String> entities(List sentGroup, Map<Long,String>lmap) {
	return (List<String>) ((Stream<String>) sentGroup.stream().
		map(s -> Json.get(s, "mentions")).
                filter(m -> m != null).map(l -> ((List) l).stream().
		    map(m -> lmap.containsKey(
                       (Long) Json.get(m,"text","beginOffset")) ? 
                       lmap.get((Long)Json.get(m,"text","beginOffset")) : 
		       (String) Json.get(m, "text", "content"))).
                reduce(Stream.empty(),
                       (a, b) -> Stream.concat((Stream)a,(Stream)b))).
	        collect(Collectors.toList());
    }

    private static List<String> entityToks(List sentGroup, 
                                           Map<Long,String> tmap) {
	return (List<String>) ((Stream<String>) sentGroup.stream().
		map(s -> Json.get(s, "mentions")).
                filter(m -> m != null).map(l -> ((List) l).stream().
		    map(m -> tmap.get((Long)Json.get(m,"text","beginOffset")))).
                reduce(Stream.empty(),
                       (a, b) -> Stream.concat((Stream)a,(Stream)b))).
	        collect(Collectors.toList());
    }

    private static Map<Long, String> tokenMap(Object json) {
        Map<Long, String> tmap = new HashMap();
        List toks = (List) Json.get(json, "tokens");
        for (Object tok : toks) {
            Object d = Json.get(tok, "dependencyEdge");
            int p = toks.indexOf(tok);
            while (d != null && !"ROOT".equals((String)Json.get(d,"label"))) {
                p = ((Long) Json.get(d, "headTokenIndex")).intValue();
                d = p<0 || p>=toks.size() ? null : 
                    Json.get(toks.get(p), "dependencyEdge");
            }
            tmap.put((Long) Json.get(tok, "text", "beginOffset"),
                     (String) Json.get(toks.get(p), "lemma"));
        }
        log.info("tokenMap.map = " + tmap);
        return tmap;
    }

    private static Map<Long, String> lemmaMap(Object json) {
        Map<Long, String> tmap = new HashMap();
        List toks = (List) Json.get(json, "tokens");
        for (Object tok : toks) {
            tmap.put((Long) Json.get(tok, "text", "beginOffset"),
                     (String) Json.get(tok, "lemma"));
        }
        log.info("lemmaMap.map = " + tmap);
        return tmap;
    }

    public static int getParseIndex(String text, Object syntax) {
        List sentences = (List) Json.get(syntax, "sentences");
        for (int i = 0; i < sentences.size(); i++) {
	    String s = (String) Json.get(sentences.get(i), "text", "content");
	    if (s != null && s.trim().contains(text))
		 return i;
	}
	return -1;
    }


    private static List<List> sentGrpsByEntity(Object entJson, List sentences,
		    int[] indices) {
	List<List> sentGrps = new ArrayList();
	List<Integer> processed = new ArrayList();
        log.info("sentGrpsByEntity().indices.len = " + indices.length);
	for (Object entity : (List) Json.get(entJson, "entities")) {
	    sentGrps.add(new ArrayList());
            for (Object mention : (List) Json.get(entity, "mentions")) {
                Object b = Json.get(mention,"text", "beginOffset");
		int s = b == null ? 0 : indices[((Long) b).intValue()];
		if (Json.get(sentences.get(s), "mentions") == null)
                    Json.put(sentences.get(s), new ArrayList(), "mentions");
                ((List) Json.get(sentences.get(s), "mentions")).add(mention);
	        if (!processed.contains(s)) { 
                    // Group by 1st mention of entity
	            sentGrps.get(sentGrps.size()-1).add(sentences.get(s));
		    processed.add(s);
		}
	    }
            log.info("sentGrpsByEntity().entity = " + entity + 
                     ", s = " + sentGrps.get(sentGrps.size()-1));
	}
        log.info("sentGrpsByEntity().processed = " + processed);
	for (Object s : sentences) {
	    if (!processed.contains(
		((Long) Json.get(s, "text", "beginOffset")).intValue())) {
		sentGrps.add(new ArrayList());
		sentGrps.get(sentGrps.size()-1).add(s);
	    }
	}
	return sentGrps.stream().filter(
			g -> !g.isEmpty()).collect(Collectors.toList());
    }

    public static int[] charIndexArray(List sentences, String text) {
	int[] indices = new int[text.length()]; int j = 0;
	for (int i = 0; i < text.length(); i++) {
	    indices[i] = j;
	    Object s = sentences.get(j);
	    if (((Long) Json.get(s, "text", "beginOffset")) + 
	        ((String) Json.get(s, "text", "content")).length()+1 <= i)
	        j++;
	}
	return indices;
    }


    private static List<Chunk> toReadmeChunks(List<String> tokens, String url) {
	List<Chunk> chunks = new ArrayList();
	List<Chunk> parents = new ArrayList();
	Chunk chunk = null;
	for (String tok : tokens) {
	    if (tok.startsWith("#")) {
	        chunk = new Chunk(tok, "");
	        chunks.add(chunk);
	        if (!parents.isEmpty()) {
	            if (level(tok) ==
		        level(parents.get(parents.size()-1).title)) 
		        parents.remove(parents.size()-1);
		    if (!parents.isEmpty())
			parents.get(parents.size()-1).addRef(chunk);
		}
		parents.add(chunk);
	    } else if (chunk != null) {
	        chunk.text += ("".equals(chunk.text)? "": "\n  ") + tok;
   	    } else chunks.add(new Chunk("", tok));
	    chunks.addAll(processLinks(tok, url, 
			      "(?i)<a\\s+href=([^>]+)>Javadoc[^<]*</a>",
                              "(?i)\\[Javadoc\\]\\(([^\\)]+)\\)",
			      "(?i)<frame\\s+src=\"([^\"]+)\"[^\\>]*>"));
        }
	return chunks;
    }
    
    public static String identifier(String longText) {
	long[] counts = LongStream.range(0, 256).map(n -> 0).toArray();
	longText = longText.toLowerCase();
	for (int i=0; i < longText.length(); i++) {
	    byte c = (byte) longText.charAt(i);
            if (c < 0 || c > counts.length) continue;
	    counts[c] += 1;
	}
        StringBuilder sb = new StringBuilder();
	for (long count : counts) 
	    if (count > 0) sb.append(sb.length()==0 ? "" : "_").append(count);
	return sb.toString();
    }

    private static List<Chunk> toHtmlChunks(List<String> tokens, String url) {
	List<Chunk> chunks = new ArrayList();
	return chunks;
    }

    private static List<Chunk> processLinks(String str, String baseUrl,
		    String... patterns) {
	List<Chunk> chunks = new ArrayList();
	if (patterns.length > 0)
	    return chunks; // TODO: update for relative links, skip for beta
	for (String pattern : patterns) {
            Pattern p = Pattern.compile(pattern);
	    Matcher m = p.matcher(str);
	    while (m.find()) {
		try {
		    log.info("Fetching "+m.group(1)+", from link " + m.group());
		    chunks.addAll(chunkify(HClient.get(m.group(1), null),
					TextType.HTML, baseUrl, null));
		} catch (Exception e) { 
		    log.error("Error retrieving link " + m.group(1), e);
		}
	    }
	}
	return chunks;
    }

    public static String stripHtml(String html) {
	StringBuilder sb = new StringBuilder();
	try {
	    ParserDelegator delegator = new ParserDelegator();
	    delegator.parse(new StringReader(html), new ParserCallback() {
	        public void handleText(char[] data, int pos) {
		    sb.append(data);
	        };
	    }, true);
	} catch (IOException e) { log.error("Error parsing HTML", e); }
	return sb.toString();
    }

    private static int level(String heading) {
	int l = 0;
	while (heading.length()>l && heading.charAt(l)=='#' && l<heading.length())  
	    l++;
	return l;
    }

}
