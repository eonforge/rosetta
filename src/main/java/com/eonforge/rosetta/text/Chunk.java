package com.eonforge.rosetta.text;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Pattern;
import java.util.stream.Collectors;
import com.eonforge.delphi.db.Client;
import com.eonforge.delphi.db.Datum;
import com.eonforge.delphi.db.model.Node;
import com.eonforge.delphi.search.Doc;
import com.eonforge.rosetta.index.Index;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class Chunk extends Datum {

    public long id = 0l;
    public String title = null;
    public String text = null;
    public String templateId = null;
    public List<Ref> refs = new ArrayList<Ref>();
    public List<Datum> data = new ArrayList<Datum>();
    public List<String> mentions = new ArrayList();
    public Map<String, String> mentionRoots = new HashMap();
    public Chunk prev = null, next = null;
    private static final Logger log = LoggerFactory.getLogger(Chunk.class);
   
    public Chunk() { this("", ""); }

    public Chunk(String title, String text) {
	this.title = title; this.text = text;
    }

    public Chunk(String title, String text, List<String> mentions, 
                 List<String> mentionRoots) {
	this(title, text);
	this.mentions.addAll(mentions);
        for (int i = 0; i < mentions.size(); i++) 
            this.mentionRoots.put(mentions.get(i), 
                mentionRoots.size()<=i? null: mentionRoots.get(i));
    }

    @Override
    public String table() { return "texts"; }

    @Override
    public Datum newInstance(Map<String, Object>... data) {
	return new Chunk();
    }

    @Override
    public Map<String, Object> colMap() {
        Map<String, Object> fields = new HashMap();
	fields.put("title", title);
	fields.put("text", text);
	fields.put("template_id", templateId);
	fields.put("refs_s", refs.stream().map(r -> r.toString()).collect(
				Collectors.joining(",")));
	fields.put("mentions_s",mentions.stream().map(r ->r.toString()).collect(
				Collectors.joining(",")));
	fields.put("roots_s", mentionRoots.entrySet().stream().map(
	    e -> e.getKey()+":"+e.getValue()).collect(Collectors.joining(",")));
	fields.put("data", this.data.stream().map(d -> 
	    d.get("id")).collect(Collectors.toList()));
	return fields;
    }

    public Doc toDoc() {
	Doc doc = new Doc("texts");
	doc.merge(dissoc(colMap(), "data"));
        doc.put("id", toUUID(this.title + this.text).toString());
	return doc;
    }

    public List<Datum> constructNodes(Index nodeIndex, Client<Node> db) {
        Map<String, List<Datum>> nodes=new HashMap(), fns=new LinkedHashMap();
        Map<String, List<String>> fparams = new HashMap();
	List<String> terms = Datum.concUnique(mentionRoots.values(), mentions);
        log.info("constructNodes.terms = " + terms + ", this = " + this);
        for (int i = 0; i < terms.size(); i++) {
	    String mention = terms.get(i);
            String q = toQuery("behaviour_ss", mention);
            if (q != null && !"".equals(q))
                q = q.replaceAll("\\*","") + " OR " +q+ " AND " + toUnionQuery(
	            "text_s", mention, terms.subList(i, terms.size()));
	    if (!"".equals(q))
	        fns.put(mention, fromDb(db,nodeIndex.search(q,"",-1,-1)));
            log.info("constructNodes.q = "+q+", fns = "+fns.get(mention));
        }
        for (String mention : mentions) {
                String q = toQuery("name", mention);
	        if (!"".equals(q)) {
	            nodes.put(mention, nodeIndex.search(q,"",-1,-1));
                    log.info("constructNodes.q = " + q + ", nodes = " + nodes);
                    if (!nodes.get(mention).isEmpty()) {
                        //if (fns.containsKey(mention)) fns.remove(mention);
                        updateParams(fns, fparams, 
                            db.fetchById(nodes.get(mention).get(0).get("id")));
                    }
                }
        }
        Datum n = toNode(fns, fparams, nodeIndex);
        return n==null ? Collections.emptyList() : Arrays.asList(n);
    }

    private static String toUnionQuery(String fld, String q, List<String> qs) {
        StringBuilder sb = new StringBuilder(fld).append(":(");
        if (q!=null && !"".equals(q)) sb.append("*").append(q).append("*");
        for (String subq : qs) 
            if (subq != null && !sb.toString().contains("*"+subq+"*") && 
                !Pattern.matches("^[0-9\\.]+$", subq))
                sb.append(sb.length()==fld.length()+2? "*": " *").append(subq).
                   append("*");
        return sb.append(")").toString();
    }

    private static String toQuery(String fld, String q, List<String> qs) {
        StringBuilder sb = new StringBuilder(toQuery(fld, q));
        for (String subq : qs) 
            sb.append(sb.length()==0 ? toQuery(fld, subq) :
                      " AND " + toQuery(fld, subq).replaceAll("^\\*", "")); 
        return sb.toString();
    }

    private static String toQuery(String fld, String q) {
	q = (q==null || q.trim().length() == 0? "" : 
             q.trim().replaceAll("[\\[\\]:]","").toLowerCase());
	return (q.trim().length() == 0? "" : fld + ":*" + q.trim() + "*");
    }

    private static List<Datum> fromDb(Client<Node> db, List<Datum> data) {
        List<Datum> res = new ArrayList();
        for (Datum d : data) 
            res.add(db.fetchById(d.get("id").toString()));
        return res;
    }

    private static String fnKey(Datum d, String fn) {
        String fkey = d==null ? "" : d.getKey("behaviour_", "(?i).*"+fn+".*");
        if (fkey == null) fkey = "";
        return fkey;
    }

    private static String inputKey(Datum d, String fn) {
        return fnKey(d, fn).replace("behaviour_", "inputs_");
    }

    public boolean updateParams(Map<String, List<Datum>> fns, 
        Map<String, List<String>> params, Datum param) {
        for (String fn : fns.keySet()) {
            Datum d = fns.get(fn).isEmpty() ? null : fns.get(fn).get(0);
            String inpK = inputKey(d, fn);
            log.info("updateParams.fn = " + fn + ", d = " + d + ", data = " +
                fns.get(fn).size()+", d.keys = " + (d==null||"".equals(inpK)? 0:
                d.getKeys(inpK).size()) + ", params="+params.get(fn));
            if (d == null) continue;
            Object v = d.getValue(inpK, "(?i).*"+param.get("name")+".*");
            log.info("updateParams.v="+v+", rgx=(?i).*"+param.get("name")+".*");
            if (param.get("name") != null && v != null ||
                d.getKeys(inpK).size() > 
                    (!params.containsKey(fn) ? 0 : params.get(fn).size())) {
                if (params.get(fn) == null)
                    params.put(fn, new ArrayList());
                params.get(fn).add(param.get("name").toString());
                log.info("updateParams.added fn=" + fn + ", " + params.get(fn));
                return true;
            }
        }
        return false;
    }

    public Datum toNode(Map<String, List<Datum>> fns, 
                        Map<String, List<String>> fparams, Index<Node> idx) {
        Node n = null;
        log.info("fparams = " + fparams.size());
        for (String m : fparams.keySet()) {
            List<String> ps = fparams.get(m);
            Datum fd = fns.get(m).get(0);
            log.info("inputs = " + fd.getKeys(inputKey(fd, m)) + 
                     ", ps = " + ps.size() + ", q = behaviour_ss:" + m +
                     ", fKey = " + fd.get(fnKey(fd, m)) + 
                     ", q1 = " + toQuery("behaviour_ss", m, ps));
            if (fd.getKeys(inputKey(fd, m)).size() != ps.size() ||
                fd.get("name") == null || m.toLowerCase().equals(
                fd.get("name").toString().toLowerCase()) ||
                fd.get(fnKey(fd, m))==null || !fd.get(fnKey(fd, m)). 
                    toString().toLowerCase().equals(m.toLowerCase()) ||
                idx.search("behaviour_ss:"+ m,"",-1,-1).isEmpty())
                continue;
            if (n == null) n = new Node();
            if (!n.containsKey("name")) n.put("name", "AnalyzerGenerated");
            String fnKey = fnKey(fd, m);
            String retKey = "retval" + fnKey.replace("behaviour", "");
            String inpKey = "inputs" + fnKey.replace("behaviour", "");
            String stepKey = "steps" + fnKey.replace("behaviour", "");
            StringBuilder sb = new StringBuilder("(");
            for (int i = 0; i < ps.size(); i++) {
                n.put(inpKey + "_" + i, ps.get(i) + " p" + i);
                sb.append(i==0 ? "": ", ").append("p").append(i);
            }
            sb.append(")");
            n.put(fnKey, fd.get(fnKey)==null ? "" : fd.get(fnKey));
            n.put(retKey, fd.get(retKey)==null ? "" : fd.get(retKey));
            String v = fd.get("name")==null ? "" : fd.get("name").toString();
            n.put(stepKey + "_0", v +" "+ v.toLowerCase() + " = new " +v+ "()");
            n.put(stepKey + "_1", "return " + v.toLowerCase() + "." + m + sb);
        }
        if (n != null) {
            n.merge("chunktitle", title, "chunktext", text);
            log.info("toNode().n = " + n);
        }
        return n;
    }

    public static List<Datum> update(List<Datum> data, Chunk chunk) {
	for (Datum d : data) 
	    d.merge("chunktitle", chunk.title, "chunktext", chunk.text,
                    "chunkmentions", chunk.mentions);
        return data;
    }

    public List<Datum> searchRelatedNodes(Index nodeIndex) {
	Chunk chunk = this;
	if (chunk.data == null) chunk.data = new ArrayList();
	String q = new HashSet<String>(chunk.mentions).stream().filter(m ->
            m!=null).collect(Collectors.joining("*"));
	q = (q == null ? "" : "*" + q.replaceAll("[\\[\\]:]", "") + "*");
	if (!"".equals(q))
	    chunk.data.addAll(nodeIndex.search("text_s:"+q,"",-1,-1));
        update(chunk.data, chunk);
        log.info("searchRelatedNodes.q = " + q + ", chunk = " + chunk);
	return chunk.data;
    }

    public List<Datum> parseDataFromNeighbour() { 
	// use prev/next chunks as template to create knode data 
	int pcount = 0, ncount = 0; 
	Chunk p = this, n = this;
	while (p.prev!=null && (p.data == null || p.data.isEmpty())) { 
	    pcount++; p = p.prev; 
	}
	while (n.next != null && (n.data == null || n.data.isEmpty())) { 
	    ncount++; n = n.next; 
	}
	Chunk chunk = pcount <= ncount ? p : n;
	if (chunk != null && chunk.data != null && !chunk.data.isEmpty()) {
	    Node node = ((Node)chunk.data.get(0)).copy(); 
	    node.merge(node.colMap());
	    bestMatchReplace(node, chunk.text, this.toVarName());
	    this.data.add(node);
	}
	return update(this.data, this);
    }

    public static String bestMatchReplace(Node n, String text, String replace) {
	String field = "", best = "", match = ""; 
	int i = 0;
	List<String> keys = new ArrayList();
	keys.add("name");
	if (n.get("fields") != null) 
	    keys.addAll((List) n.get("fields"));
	while (n.get("behaviour_" + i) != null) 
	    keys.add("behaviour_" + (i++)); 
	for (String key : keys) {
	    match = getMatch((String) n.get(key), text);
	    if (match.length() > best.length()) { best = match; field = key; }
	}
	if (best.length() > 0)
	    n.put(field, ((String) n.get(field)).replace(best, replace));
	return field;
    }

    public static String getMatch(String t1, String t2) {
	if (t1 == null || t2 == null) return "";
	t1 = t1.replaceAll("\\s", "");
	t2 = t2.replaceAll("\\s", "");
	if (containsFuzzy(t1, t2)) return t2;
	else if (containsFuzzy(t2, t1)) return t1;
	return "";
    }

    public static boolean containsFuzzy(String t1, String t2) {
	for (int i = 0; i < t1.length() - t2.length(); i++) 
	    if (equalsFuzzy(t1.substring(i, i+t2.length()), t2))
		 return true;
	return false;
    }

    public static boolean equalsFuzzy(String t1, String t2) {
	int count = 0;
	for (int i = 0; i < t1.length(); i++) {
            if (t1.charAt(i) != t2.charAt(i)) count++;
	    if (count > 1) return false;
	}
	return true;
    }

    public String toVarName() {
	String var = text.contains("\n") ? text.split("\n")[0] : text;
	for (String mention : mentions)
	    if (mention != null)
	        var = var.replace(mention, "");
	return var.replaceAll("\\s", "");
    }

    public String toString() {
        return colMap().toString();
    }

    public void addRef(Chunk chunk) { this.refs.add(new Ref(chunk)); }

    public void addRef(Chunk chunk, RefType t) { 
	this.refs.add(new Ref(chunk, t));
    }

    public enum RefType { SUB_TOPIC, REFERENCE }

    public class Ref {
	RefType type = RefType.SUB_TOPIC;
	Chunk other;
	public Ref(Chunk chunk) { other = chunk; }
	public Ref(Chunk chunk, RefType type) { 
		other = chunk; this.type = type;
	}
	
	public String toString() {
	    return type + ":" + other.id;
	}
    }
}
