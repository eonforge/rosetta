package com.eonforge.rosetta;

import java.util.HashMap;
import java.util.Map;
import com.eonforge.peregrine.Config;
import com.eonforge.peregrine.http.HClient;

/**
 * Tester class
 */
public class App 
{
    public static void main( String[] args )
    {
	Map<String, Object> req = new HashMap();
	Map<String, Object> m = new HashMap<String, Object>();
	req.put("document", (Object) m);
	req.put("encodingType", (Object) "UTF8");
	m.put("type", (Object) "PLAIN_TEXT");
	m.put("language", (Object) "EN");
	m.put("content", (Object) "Lawrence of Arabia' is a highly rated film" +
              " biography about British Lieutenant T.E. Lawrence. " +
              "Peter O'Toole plays Lawrence in the film.");
	try {
	    Config.load("rosetta.properties");
	    System.out.println("Entities:" +
                HClient.post(Config.getProperty("google-entities-url"), req));
	} catch (Exception e) { e.printStackTrace(); }
    }
}
