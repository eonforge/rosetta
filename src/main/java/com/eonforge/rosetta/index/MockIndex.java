package com.eonforge.rosetta.index;

import java.io.IOException;
import java.io.File;
import java.net.URLConnection;
import java.util.Arrays;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.HashMap;
import java.util.Map;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

import com.eonforge.delphi.db.Datum;
import com.eonforge.peregrine.Config;
import com.eonforge.peregrine.parse.Json;

import org.apache.solr.client.solrj.SolrClient;
import org.apache.solr.client.solrj.SolrQuery;
import org.apache.solr.client.solrj.SolrServerException;
import org.apache.solr.client.solrj.impl.HttpSolrClient;
import org.apache.solr.client.solrj.impl.CloudSolrClient;
import org.apache.solr.client.solrj.request.ContentStreamUpdateRequest;
import org.apache.solr.client.solrj.request.AbstractUpdateRequest;
import org.apache.solr.client.solrj.response.FacetField;
import org.apache.solr.client.solrj.response.FacetField.Count;
import org.apache.solr.client.solrj.response.IntervalFacet;
import org.apache.solr.client.solrj.response.RangeFacet;
import org.apache.solr.client.solrj.response.QueryResponse;
import org.apache.solr.client.solrj.response.UpdateResponse;
import org.apache.solr.common.SolrDocument;
import org.apache.solr.common.SolrInputDocument;
import org.apache.solr.common.util.NamedList;
import org.apache.solr.handler.extraction.ExtractingParams;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class MockIndex<T extends Datum> {

    public Map<String, List<T>> index = new HashMap();

    private SolrClient solr;

    private Map<String, SolrClient> clients = new HashMap();

    private T seed;

    private static String VFS = Config.getPathProperty("vfs-dir");

    private static final Logger log = LoggerFactory.getLogger(MockIndex.class);

    public MockIndex() {
	seed = null; solr = null; 
        String url = Config.getProperty("solr-url"); 
	for (String col : Arrays.asList("docs", "nonprofit", "notifs"))
	    clients.put(col, new HttpSolrClient.Builder(url+"/"+col).build());
    }

    public MockIndex(T seed) {
	this();
	this.seed = seed;
        String url = Config.getProperty("solr-url"); 
	if (url == null || url.length()==0) {
	    log.error("MockIndex solr url is empty");
	    return;
	}
	if (Pattern.matches("http.?://[^:]+:[0-9]+/solr.*", url)) {
	    String collection = seed.table() == null? "": 
		seed.table().replaceAll(".*\\.", "");
	    solr = clients.containsKey(collection)? clients.get(collection):
		new HttpSolrClient.Builder(url + "/" + collection).build();
	    clients.put(seed.table(), solr);
	} else {
	    solr = new CloudSolrClient.Builder().withZkHost(url).build();
	}
    }

    public boolean containsKey(String key) { return index.containsKey(key); }

    public void add(T d) {
	log.trace("add.d = " + d + ", solr = " + solr + ", table = " + d.table());
	if (solr == null) {
	    if (((HashMap)d).get("keywords")==null) return;
	    for (String key : ((String) ((HashMap)d).get("keywords"))
		.split(",")) {
		if (!index.containsKey(key)) index.put(key, new ArrayList());
		index.get(key).add(d);
	    }
	} else {
	    if ("docs".equals(d.table())) {
		addWithExtractor(d);
	    } else {
		addInputDoc(d);
	    }
	}
    }

    public NamedList<Object> addWithExtractor(T d) {
	try {
	    ContentStreamUpdateRequest req = new ContentStreamUpdateRequest(
		    "/update/extract");
	    log.info("Mime type = " + mimeType(""+d.get("id")));
	    req.addFile(new File(VFS + d.get("id")), mimeType(VFS+d.get("id")));
	    //req.setParam(ExtractingParams.EXTRACT_ONLY, "true");
	    req.setParam("literal.id", (String) d.get("id"));
	    req.setParam("uprefix", "attr_");
	    req.setParam("fmap.content", "attr_content");
	    req.setAction(AbstractUpdateRequest.ACTION.COMMIT, true, true);
	    return solr.request(req);
	} catch (SolrServerException|IOException e) {
	    log.error("Error on solr query", e);
	}
	return null;
    }

    private String mimeType(String filename) {
	if (Pattern.matches(".*\\.do[ct]$", filename)) 
	    return "application/msword";
	else if (Pattern.matches(".*\\.do[ct]x$", filename)) 
	    return "application/vnd.openxmlformats-officedocument." +
		    "wordprocessingml.document";
	else if (Pattern.matches(".*\\.xl[sta]$", filename))
	    return "application/vnd.ms-excel";
	else if (Pattern.matches(".*\\.xl[sta]x$", filename))
	    return "application/vnd.openxmlformats-officedocument." +
		    "spreadsheetml.sheet";
	else if (Pattern.matches(".*\\.p[op][sta]$", filename))
	    return "application/vnd.ms-powerpoint";
	else if (Pattern.matches(".*\\.p[op][sta]x$", filename))
	    return "application/vnd.openxmlformats-officedocument." +
		    "presentationml.presentation";
        return URLConnection.guessContentTypeFromName(filename);
    }

    public void addInputDoc(T d) {
	SolrInputDocument doc = new SolrInputDocument();
	for (Map.Entry<String, Object> entry : d.colMap().entrySet())
	    doc.addField(entry.getKey(), entry.getValue());
	try {
	    UpdateResponse resp = solr.add(doc);
	    solr.commit();
	    log.info("Doc added to solr: {}", doc);
	} catch (SolrServerException|IOException e) { 
	    log.error("Error adding doc", e);
	}
    }

    public void addAll(List<T> data) {
	for (T d : data) add(d);
    }

    public List<T> search(String text) {
	if (solr == null) {
	    List<T> results = new ArrayList();
	    if (text == null) return results;
	    Map<T, Integer> counts = new HashMap();
	    String[] q = text.replaceAll("[^a-zA-Z0-9]", "").split("[\\s]+");
	    for (String word : q)
		if (index.containsKey(word)) 
		    for (T d : index.get(word)) 
			counts.put(d, counts.containsKey(d)? 
				counts.get(d)+1: 1);
	    results.addAll(counts.keySet());
	    Collections.sort(results, new Comparator<T>() {
		@Override
		public int compare(T a, T b) {
		    return counts.get(a) - counts.get(b);
		}
	    });
	    return results;
	}
	return Collections.emptyList();
    }

    public Object searchRaw(Map<String, String> params) {
        if (solr != null) {
	    String collection = params.get("collection");
	    if (collection == null) { 
		log.info("No collection in solr query " + params);
		return null;
	    }
	    SolrQuery qry = new SolrQuery();
	    for (String key : params.keySet())
		qry.set(key, params.get(key));
	    try {
		QueryResponse response = clients.get(collection).query(qry);
	        Object res = Json.toJson(toMap(response, params));
		log.trace("searchRaw.result = " + res);
		return res;
	    } catch (SolrServerException|IOException|NullPointerException e) {
		log.error("Error on solr query", e);
	    }
	}
	return null;
    }

    private Map toMap(QueryResponse response, Map<String, String> params) {
	Map map = (Map) Json.toJson(response.getResponse());
	Object docs = map.get("response");
	Map resp = new HashMap();
	resp.put("numFound", ((Collection) docs).size());
	resp.put("docs", docs);
	resp.put("start", params.containsKey("start")? params.get("start"): 0);
	map.put("response", resp);
	Map fmap = new HashMap();
	fmap.put("facet_queries", response.getFacetQuery()==null? 
		new HashMap(): Json.toJson(response.getFacetQuery()));
	fmap.put("facet_fields", fToJson(response.getFacetFields()));
	fmap.put("facet_ranges", rToJson(response.getFacetRanges()));
	fmap.put("facet_intervals", iToJson(response.getIntervalFacets()));
	map.put("facet_counts", fmap);
        return map;
    }

    private Map<String, List> fToJson(List<FacetField> fields) {
        Map<String, List> map = new HashMap();
	if (fields == null) return map;
	for (FacetField field : fields) {
	    map.put(field.getName(), new ArrayList());
	    for (FacetField.Count count : field.getValues())
	       map.get(field.getName()).addAll(Arrays.asList(
		  count.getName(), count.getCount()));
	}
	return map; 
    }

    private Map<String, List> rToJson(List<RangeFacet> fields) {
        Map<String, List> map = new HashMap();
	if (fields == null) return map;
	for (RangeFacet field : fields) {
	    map.put(field.getName(), new ArrayList());
	    for (RangeFacet.Count count : 
			    (List<RangeFacet.Count>) field.getCounts()) {
	       Map fmap = new HashMap();
	       fmap.put(count.getValue(), count.getCount());
	       map.get(field.getName()).add(fmap);
	    }
	}
	return map; 
    }

    private Map<String, List> iToJson(List<IntervalFacet> fields) {
        Map<String, List> map = new HashMap();
	if (fields == null) return map;
	for (IntervalFacet field : fields) {
	    map.put(field.getField(), new ArrayList());
	    for (IntervalFacet.Count count : field.getIntervals()) {
	       Map fmap = new HashMap();
	       fmap.put(count.getKey(), count.getCount());
	       map.get(field.getField()).add(fmap);
	    }
	}
	return map; 
    }

    public List<T> search(String q, String sort, int start, int rows) {
        if (solr != null) {
	    SolrQuery qry = new SolrQuery();
	    qry.set("q", q);
	    qry.set("sort", sort);
	    qry.set("start", start);
	    qry.set("rows", rows);
	    try {
		QueryResponse response = solr.query(qry);
		return (List<T>) response.getResults().stream().map(
		    d -> seed.newInstance((Map) d)
		    ).collect(Collectors.toList());
	    } catch (SolrServerException|IOException e) {
		log.error("Error on solr query", e);
	    }
	}
	return Collections.emptyList();
    }

}
