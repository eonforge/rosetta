package com.eonforge.rosetta.index;

import java.io.IOException;
import java.io.File;
import java.net.URLConnection;
import java.util.Arrays;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

import com.eonforge.delphi.db.Datum;
import com.eonforge.peregrine.Config;
import com.eonforge.peregrine.parse.Json;

import org.apache.solr.client.solrj.SolrClient;
import org.apache.solr.client.solrj.SolrQuery;
import org.apache.solr.client.solrj.SolrServerException;
import org.apache.solr.client.solrj.impl.HttpSolrClient;
import org.apache.solr.client.solrj.impl.CloudSolrClient;
import org.apache.solr.client.solrj.request.ContentStreamUpdateRequest;
import org.apache.solr.client.solrj.request.AbstractUpdateRequest;
import org.apache.solr.client.solrj.response.FacetField;
import org.apache.solr.client.solrj.response.FacetField.Count;
import org.apache.solr.client.solrj.response.IntervalFacet;
import org.apache.solr.client.solrj.response.RangeFacet;
import org.apache.solr.client.solrj.response.QueryResponse;
import org.apache.solr.client.solrj.response.UpdateResponse;
import org.apache.solr.common.SolrDocument;
import org.apache.solr.common.SolrInputDocument;
import org.apache.solr.common.util.NamedList;
import org.apache.solr.handler.extraction.ExtractingParams;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class Index<T extends Datum> {

    public Map<String, List<T>> index = new HashMap();

    public int maxInflightQueries = 1;

    private SolrClient solr;

    private Map<String, SolrClient> clients = new HashMap();

    private Map<String, Integer> hostQueries = new HashMap();

    private T seed;

    private int connectionTimeout = 300;

    private int socketTimeout = 300;

    private static String VFS = Config.getPathProperty("vfs-dir");

    private static final Logger log = LoggerFactory.getLogger(Index.class);

    public Index() {
	seed = null; solr = null; 
        String url = Config.getProperty("solr-url"); 
	for (String col : Arrays.asList("docs", "nonprofit", "notifs"))
	    clients.put(col, new HttpSolrClient.Builder(url+"/"+col).build());
                    //withConnectionTimeout(connectionTimeout).
                    //withSocketTimeout(socketTimeout).build());
    }

    public Index(T seed) {
	this();
	this.seed = seed;
        String url = Config.getProperty("solr-url"); 
	if (url == null || url.length()==0) {
	    log.error("Index solr url is empty");
	    return;
	}
	if (Pattern.matches("http.?://[^:]+:[0-9]+/solr.*", url)) {
	    String collection = seed.table() == null? "": 
		seed.table().replaceAll(".*\\.", "");
	    solr = clients.containsKey(collection)? clients.get(collection):
		new HttpSolrClient.Builder(url + "/" + collection).build();
                    //withConnectionTimeout(connectionTimeout).
                    //withSocketTimeout(socketTimeout).build();
	    clients.put(seed.table(), solr);
	} else {
	    solr = new CloudSolrClient.Builder().withZkHost(url).build();
	}
    }

    public boolean setParams(String key, int value) {
        try {
            if (solr != null) setParam(solr, key, value);
            for (SolrClient core : clients.values())
                setParam(core, key, value);
        } catch (Exception e) { 
            log.error("Error setting param: " + key + "=" + value, e);
            return false;
        }
        return true;
    }

    public void setParam(SolrClient client, String key, int value) {
        if (client instanceof HttpSolrClient)
            ((HttpSolrClient) client).getInvariantParams().set(key, value);
    }

    public SolrClient getClient(String collection) {
        if (!clients.containsKey(collection)) {
            String url = Config.getProperty("solr-url"); 
	    clients.put(collection, 
                new HttpSolrClient.Builder(url+"/"+collection).build());
        }
        return clients.get(collection);
    }

    public boolean containsKey(String key) { return index.containsKey(key); }

    public boolean add(T d) {
	log.trace("add.d = " + d.get("id") +", solr = " + solr + 
                 ", table = " + d.table());
	if (solr == null) {
	    if (((HashMap)d).get("keywords")==null) return false;
	    for (String key : ((String) ((HashMap)d).get("keywords"))
		.split(",")) {
		if (!index.containsKey(key)) index.put(key, new ArrayList());
		index.get(key).add(d);
	    }
	    return true;
	} else {
	    if ("docs".equals(d.table())) {
		return addWithExtractor(d) != null;
	    } else {
		return addInputDoc(d) != 0;
	    }
	}
    }

    public NamedList<Object> addWithExtractor(T d) {
	try {
	    ContentStreamUpdateRequest req = new ContentStreamUpdateRequest(
		    "/update/extract");
	    log.info("Mime type = " + mimeType(""+d.get("id")));
	    req.addFile(new File(VFS + d.get("id")), mimeType(VFS+d.get("id")));
	    //req.setParam(ExtractingParams.EXTRACT_ONLY, "true");
	    req.setParam("literal.id", (String) d.get("id"));
	    req.setParam("uprefix", "attr_");
	    req.setParam("fmap.content", "attr_content");
	    req.setAction(AbstractUpdateRequest.ACTION.COMMIT, true, true);
	    return solr.request(req);
	} catch (SolrServerException|IOException e) {
	    log.error("Error on solr query", e);
	}
	return null;
    }

    private String mimeType(String filename) {
	if (Pattern.matches(".*\\.do[ct]$", filename)) 
	    return "application/msword";
	else if (Pattern.matches(".*\\.do[ct]x$", filename)) 
	    return "application/vnd.openxmlformats-officedocument." +
		    "wordprocessingml.document";
	else if (Pattern.matches(".*\\.xl[sta]$", filename))
	    return "application/vnd.ms-excel";
	else if (Pattern.matches(".*\\.xl[sta]x$", filename))
	    return "application/vnd.openxmlformats-officedocument." +
		    "spreadsheetml.sheet";
	else if (Pattern.matches(".*\\.p[op][sta]$", filename))
	    return "application/vnd.ms-powerpoint";
	else if (Pattern.matches(".*\\.p[op][sta]x$", filename))
	    return "application/vnd.openxmlformats-officedocument." +
		    "presentationml.presentation";
        return URLConnection.guessContentTypeFromName(filename);
    }

    public int addInputDoc(T d) {
	SolrInputDocument doc = new SolrInputDocument();
	for (Map.Entry<String, Object> entry : d.entrySet()) {
	    if (entry.getValue() instanceof Map) 
		for (Map.Entry<String, Object> ent : 
			((Map<String, Object>) entry.getValue()).entrySet())
	            addField(doc, ent.getKey(), format(ent.getValue()));
	    else addField(doc, entry.getKey(), format(entry.getValue()));
	}
	return sendInputDoc(doc);
    }

    public static void addField(SolrInputDocument doc, String k, Object v) {
        String suffix = "id".equals(k) ? "" : "_s";
        if (suffix.length() > 0 && v instanceof Collection) 
            suffix += "s";
	doc.addField(k + suffix, v);
    }

    public boolean deleteByQuery(String q) {
        return deleteObj(q);
    }

    public boolean delete(T d) {
        return deleteObj(d);
    }

    private boolean deleteObj(Object d) {
	try {
	    log.info("delete.d = " + d);
	    UpdateResponse resp = d instanceof String ? 
                solr.deleteByQuery((String) d) : 
                solr.deleteById(((Map) d).get("id").toString());
	    solr.commit();
	    log.info("Doc deleted from solr: {}", resp);
	    return resp.getStatus() == 200;
	} catch (SolrServerException|IOException e) { 
	    log.error("Error deleting doc", e);
	}
	return false;
    }

    private Object format(Object val) {
        if (val instanceof UUID) return val.toString();
        else return val;
    }

    public int update(T d) {
	SolrInputDocument doc = new SolrInputDocument();
	doc.addField("id", d.get("id"));
	for (Map.Entry<String, Object> entry : d.entrySet()) {
	    if ("id".equals(entry.getKey())) continue;
	    if (entry.getValue() instanceof Map) 
		for (Map.Entry<String, Object> ent : 
		    ((Map<String, Object>) entry.getValue()).entrySet())
	            setField(doc, ent.getKey(), ent.getValue());
	    else setField(doc, entry.getKey(), entry.getValue());
	}
	return sendInputDoc(doc);
    }

    public int sendInputDoc(SolrInputDocument doc) {
	try {
	    log.trace("sendInputDoc.doc = " + doc);
	    UpdateResponse resp = solr.add(doc);
	    solr.commit();
	    log.trace("Doc added to solr: {} = {}", doc.get("id"), resp);
	    return resp.getStatus();
	} catch (SolrServerException|IOException e) { 
	    log.error("Error adding doc", e);
	}
	return 1;
    }

    private void setField(SolrInputDocument doc, String key, Object value) {
        Map<String, Object> mod = new HashMap(1);
        mod.put("set", format(value));
        addField(doc, String.valueOf(key), mod);
    }

    public void addAll(List<T> data) {
	for (T d : data) add(d);
    }

    public List<T> search(String text) {
	if (solr == null) {
	    List<T> results = new ArrayList();
	    if (text == null) return results;
	    Map<T, Integer> counts = new HashMap();
	    String[] q = text.replaceAll("[^a-zA-Z0-9]", "").split("[\\s]+");
	    for (String word : q)
		if (index.containsKey(word)) 
		    for (T d : index.get(word)) 
			counts.put(d, counts.containsKey(d)? 
				counts.get(d)+1: 1);
	    results.addAll(counts.keySet());
	    Collections.sort(results, new Comparator<T>() {
		@Override
		public int compare(T a, T b) {
		    return counts.get(a) - counts.get(b);
		}
	    });
	    return results;
	}
	return Collections.emptyList();
    }

    public Map searchRaw(Map<String, String> params) {
        if (solr != null) {
	    String collection = params.containsKey("collection") ? 
		params.get("collection") : params.get("core");
	    if (collection == null) { 
		log.info("No collection in solr query " + params);
		return Collections.emptyMap();
	    }
	    SolrQuery qry = new SolrQuery();
	    for (String key : params.keySet())
		qry.set(key, params.get(key));
	    try {
		QueryResponse response = getClient(collection).query(qry);
	        Object res = Json.toJson(toMap(response, params));
		log.info("searchRaw.qry = " + qry +", qry.result = " + res);
		return (Map) res;
	    } catch (SolrServerException|IOException|NullPointerException e) {
		log.error("Error on solr query", e);
	    }
	}
	return Collections.emptyMap();
    }

    private Map toMap(QueryResponse response, Map<String, String> params) {
	Map map = (Map) Json.toJson(response.getResponse());
	Object docs = fromSolr(map.get("response"));
	Map resp = new HashMap();
	resp.put("numFound", ((Collection) docs).size());
	resp.put("docs", docs);
	resp.put("start", params.containsKey("start")? params.get("start"): 0);
	map.put("response", resp);
	Map fmap = new HashMap();
	fmap.put("facet_queries", response.getFacetQuery()==null? 
		new HashMap(): Json.toJson(response.getFacetQuery()));
	fmap.put("facet_fields", fToJson(response.getFacetFields()));
	fmap.put("facet_ranges", rToJson(response.getFacetRanges()));
	fmap.put("facet_intervals", iToJson(response.getIntervalFacets()));
	map.put("facet_counts", fmap);
        return map;
    }

    private static Collection fromSolr(Object data) {
        Collection docs = new ArrayList();
        for (SolrDocument doc : (Collection<SolrDocument>) data) {
            for (String k : doc.getFieldNames()) {
                String k1 = k.replaceAll("_s.?$", "");
                doc.put(k1, doc.getFieldValue(k));
                doc.remove(k);
            }
            docs.add(doc);
        }
        return docs;
    }
    private Map<String, List> fToJson(List<FacetField> fields) {
        Map<String, List> map = new HashMap();
	if (fields == null) return map;
	for (FacetField field : fields) {
	    map.put(field.getName(), new ArrayList());
	    for (FacetField.Count count : field.getValues())
	       map.get(field.getName()).addAll(Arrays.asList(
		  count.getName(), count.getCount()));
	}
	return map; 
    }

    private Map<String, List> rToJson(List<RangeFacet> fields) {
        Map<String, List> map = new HashMap();
	if (fields == null) return map;
	for (RangeFacet field : fields) {
	    map.put(field.getName(), new ArrayList());
	    for (RangeFacet.Count count : 
			    (List<RangeFacet.Count>) field.getCounts()) {
	       Map fmap = new HashMap();
	       fmap.put(count.getValue(), count.getCount());
	       map.get(field.getName()).add(fmap);
	    }
	}
	return map; 
    }

    private Map<String, List> iToJson(List<IntervalFacet> fields) {
        Map<String, List> map = new HashMap();
	if (fields == null) return map;
	for (IntervalFacet field : fields) {
	    map.put(field.getField(), new ArrayList());
	    for (IntervalFacet.Count count : field.getIntervals()) {
	       Map fmap = new HashMap();
	       fmap.put(count.getKey(), count.getCount());
	       map.get(field.getField()).add(fmap);
	    }
	}
	return map; 
    }

    public List<T> search(String q, String sort, int start, int rows) {
        if (solr != null) {
	    SolrQuery qry = new SolrQuery();
	    qry.set("q", q);
	    qry.set("sort", sort);
	    qry.set("start", start < 1 ? 0 : start);
	    qry.set("rows", rows < 1 ? 10 : rows);
	    try {
		QueryResponse response = solr.query(qry);
		return (List<T>) fromSolr(response.getResults()).stream().map(
		    d -> (Datum) seed.newInstance((Map) d)
		    ).collect(Collectors.toList());
	    } catch (SolrServerException|IOException e) {
		log.error("Error on solr query", e);
	    }
	}
	return Collections.emptyList();
    }

    private void ensureHostAvailable() {
        try {
            while (hostsBusy())
                Thread.sleep(5000);
        } catch (InterruptedException e) {
            log.error("Error waiting for host", e);
        }
    }

    private String getUrl(SolrClient client) {
        return client instanceof HttpSolrClient ?
            ((HttpSolrClient) client).getBaseURL() :
            ((CloudSolrClient) client).getZkHost();
    }

    public boolean hostsBusy() {
        if (hostQueries.isEmpty()) {
            hostQueries.put(getUrl(solr), 0);
            for (SolrClient client : clients.values())
                hostQueries.put(getUrl(client), 0);
            log.info("hostsBusy().inflights = " + hostQueries);
        }
        log.info("hostsBusy().inflights.1 = " + hostQueries);
        for (String url : hostQueries.keySet()) 
            if (hostQueries.get(url) < maxInflightQueries) {
                hostQueries.put(url, hostQueries.get(url) + 1);
                return false;
            }
        return true;
    }
}
