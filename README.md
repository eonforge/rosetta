# Rosetta - Natural language to code metamodel mapping

The [Rosetta Stone](https://en.wikipedia.org/wiki/Rosetta_Stone) is a granodiorite stele, found in 1799, inscribed with three versions of a decree issued at Memphis, Egypt in 196 BC during the Ptolemaic dynasty on behalf of King Ptolemy V. The top and middle texts are in Ancient Egyptian using hieroglyphic script and Demotic script, respectively, while the bottom is in Ancient Greek.

## Compiling and Running

This is a maven project, so the usual maven targets apply:
```
mvn clean package [test]
```

## Updating maven repository

For development purposes, updating snaphots is necessary so changes are picked up by dependent projects. 
```
mvn clean install -U
or
mvn dependency:purge-local-repository clean package
```
